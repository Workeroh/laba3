#include "functions.h"
#include <cmath>
#include <stdlib.h>


/*!
*\file
*\brief Основные функции программы.
*\author Alex
*\version 1.0
*
*/


/*!
 * @brief Функция проверяет корректность введённых данных
 * @param x
 * @param y
 * @return int
 * @code
 *  int a = 0;
    char c = '\0';
    while (scanf("%d", &a) != 1 || a <= x || a >= y)
    {
        while (scanf("%c", &c) != 0 && c != '\n');
        printf("Invalid value entered! Try again!\n");
    }
    return a;
 * @endcode
 */

int check(int x, int y)
{
    int a = 0;
    char c = '\0';
    while (scanf("%d", &a) != 1 || a <= x || a >= y)
    {
        while (scanf("%c", &c) != 0 && c != '\n');
        printf("Invalid value entered! Try again!\n");
    }
    return a;
}


/*!
 * @brief Задача функции заключается в решении линейного уравнения
 * @param a
 * @param b
 * @return float
 * @code
 *  float x = 0;
    while(a == 0)
    {
        if(a == 0)
        {
            printf("Error! a = 0!");
            printf("Enter a: ");
            a = check(-99999, 99999);
            printf("Enter b: ");
            b = check(-99999, 99999);
        }
    }

    x = -(static_cast<float>(b/a));

    return x;
 * @endcode
 */

float linearEq(int a, int b)
{
    float x = 0;
    while(a == 0)
    {
        if(a == 0)
        {
            printf("Error! a = 0!");
            printf("Enter a: ");
            a = check(-99999, 99999);
            printf("Enter b: ");
            b = check(-99999, 99999);
        }
    }

    x = -(static_cast<float>(b/a));

    return x;
}


/*!
 * @brief Задача функции заключается в решении квадратного уравнения
 * @param a
 * @param b
 * @param c
 * @return float*
 * @code
 *  int D = -1;
    float* x = new float[2];

    if(a == 0)
    {
        while(b == 0)
        {
            if(b == 0)
            {
                printf("Error! b = 0!");
                printf("Enter b: ");
                b = check(-99999, 99999);
                printf("Enter c: ");
                c = check(-99999, 99999);
            }
        }

        x[0] = -(static_cast<float>(c/b));
    }

    while(D < 0)
    {
        D = (b*b) - (4*a*c);

        if(D == 0)
        {
            x[0] = (-b/2*a);
            x[1] = x[0];
        }
        if(D > 0)
        {
            x[0] = (-b + sqrt(D))/2;
            x[1] = (-b - sqrt(D))/2;
        }
        if(D < 0)
        {
            printf("Error! D < 0!\n");
            printf("Enter a: ");
            a = check(-99999, 99999);
            printf("Enter b: ");
            b = check(-99999, 99999);
            printf("Enter c: ");
            c = check(-99999, 99999);
        }
    }
    return x;
 * @endcode
 */

float* quadrEq(int a, int b, int c)
{
    int D = -1;
    float* x = new float[2];

    if(a == 0)
    {
        while(b == 0)
        {
            if(b == 0)
            {
                printf("Error! b = 0!");
                printf("Enter b: ");
                b = check(-99999, 99999);
                printf("Enter c: ");
                c = check(-99999, 99999);
            }
        }

        x[0] = -(static_cast<float>(c/b));
    }

    while(D < 0)
    {
        D = (b*b) - (4*a*c);

        if(D == 0)
        {
            x[0] = (-b/2*a);
            x[1] = x[0];
        }
        if(D > 0)
        {
            x[0] = (-b + sqrt(D))/2;
            x[1] = (-b - sqrt(D))/2;
        }
        if(D < 0)
        {
            printf("Error! D < 0!\n");
            printf("Enter a: ");
            a = check(-99999, 99999);
            printf("Enter b: ");
            b = check(-99999, 99999);
            printf("Enter c: ");
            c = check(-99999, 99999);
        }
    }
    return x;
}


/*!
 * @brief arrCheck
 * @param x
 * @param y
 * @return bool
 * @code
 *     bool check = true;
    for (int i = 0 ; i < 2; i++)
    {
        if(x[i] != y[i])
        {
            check = false;
        }
    }
    return check;
 * @endcode
 */

bool arrCheck(float* x, float* y)
{
    bool check = true;
    for (int i = 0 ; i < 2; i++)
    {
        if(x[i] != y[i])
        {
            check = false;
        }
    }
    return check;
}


/*!
 * @brief Функция находит минимальный элемент массива
 * @param a
 * @param n
 * @return int
 * @code
 *  int j = 0;

    for (int i = 0; i < n; i++)
    {
         if (a[j] >= a[i])
         {
             j = i;
         }
    }

    return j;
 * @endcode
 */

int arrayMin(int* a, int n)
{
    int j = 0;

    for (int i = 0; i < n; i++)
    {
         if (a[j] >= a[i])
         {
             j = i;
         }
    }

    return j;
}


/*!
 * @brief Нахождение конкретного элемента арифметической прогрессии по формуле
 * @param a1
 * @param d
 * @param i
 * @return int
 * @code
 *    int a = 0;
    while(d == 0)
    {
        printf("Error!\n");
        printf("Enter d: ");
        d = check(-99999, 99999);
    }

    a = a1 + ((i - 1) * d);
    return a;
 * @endcode
 */

int progressEl(int a1, int d, int i)
{
    int a = 0;
    while(d == 0)
    {
        printf("Error!\n");
        printf("Enter d: ");
        d = check(-99999, 99999);
    }

    a = a1 + ((i - 1) * d);
    return a;
}


/*!
 * @brief Нахождение суммы арифметической прогрессии
 * @param a1
 * @param d
 * @param i
 * @return int
 * @code
 *       int s = 0;
    int a = 0;
    while(d == 0)
    {
        printf("Error!\n");
        printf("Enter d: ");
        d = check(-99999, 99999);
    }

    a = a1 + ((i - 1) * d);

    s = ((a1 + a)*i)/2;
    return s;
 * @endcode
 */

int amountAr(int a1, int d, int i)
{
    int s = 0;
    int a = 0;
    while(d == 0)
    {
        printf("Error!\n");
        printf("Enter d: ");
        d = check(-99999, 99999);
    }

    a = a1 + ((i - 1) * d);

    s = ((a1 + a)*i)/2;
    return s;
}


/*!
 * @brief Нахождение суммы геометрической прогрессии
 * @param b1
 * @param q
 * @param i
 * @return int
 * @code
 *   int s = 0;
    int b = 0;
    while(q == 0)
    {
        printf("Error!\n");
        printf("Enter q: ");
        q = check(-99999, 99999);
    }

    b = (b1 * (pow(q, i-1)));

    s = ((b*q) - b1)/(q-1);
    return s;
 * @endcode
 */

int amountG(int b1, int q, int i)
{
    int s = 0;
    int b = 0;
    while(q == 0)
    {
        printf("Error!\n");
        printf("Enter q: ");
        q = check(-99999, 99999);
    }

    b = (b1 * (pow(q, i-1)));

    s = ((b*q) - b1)/(q-1);
    return s;
}


