#ifndef TST_TEST_H
#define TST_TEST_H

#include <QObject>

class Test : public QObject
{
    Q_OBJECT

public:
    Test();

private slots:
    void test_linearEq();
    void test_quadrEq();
    void test_arrayMin();
    void test_progressEl();
    void test_amountAr();
    void test_amountG();
};

#endif // TST_TEST_H

//1
//2
//3
//4
//5
//6
//7
//8
//x
