#include <QtTest>
#include <../functions.h>
#include <tst_test.h>

// add necessary includes here



Test::Test()
{

}


void Test::test_linearEq()
{
    int a = 10;
    int b = 10;
    QCOMPARE(-1, linearEq(a, b));
}

void Test::test_quadrEq()
{
    float* x = new float[2];
    x[0] = 1;
    x[1] = -4;
    int a = 1;
    int b = 3;
    int c = -4;
    float* y = quadrEq(a, b ,c);
    QCOMPARE(true, arrCheck(x, y));
}

void Test::test_arrayMin()
{
    int* a = new int[4];
    a[0] = 7;
    a[1] = 1;
    a[2] = 10;
    a[3] = 3;
    QCOMPARE(1, arrayMin(a, 4));
}

void Test::test_progressEl()
{
    int a1 = 1;
    int d = 1;
    int i = 4;
    QCOMPARE(4, progressEl(a1, d, i));
}

void Test::test_amountAr()
{
    int a1 = 1;
    int d = 1;
    int i = 4;
    QCOMPARE(10, amountAr(a1, d, i));
}

void Test::test_amountG()
{
    int b1 = 1;
    int q = 2;
    int i = 4;
    QCOMPARE(15, amountG(b1, q, i));
}


//dfdf
//asdasd
//ok
//if
//f
//ff
//fff
//ffff
//fffff
//ffffff
//i
