#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#endif // FUNCTIONS_H

#include <stdio.h>

float linearEq(int a, int b);
int check(int x, int y);
float* quadrEq(int a, int b, int c);
bool arrCheck(float* x, float* y);
int arrayMin(int* a, int n);
int progressEl(int a1, int d, int i);
int amountAr(int a1, int d, int i);
int amountG(int b1, int q, int i);

