#include <QCoreApplication>
#include <stdlib.h>
#include <conio.h>
#include <QTextCodec>
#include "functions.h"

/**

\mainpage Главная страница

Данный проект является многозадачной программой.

*/


/**
*\file
*\brief Выполнения африметических задач.
*\author Alex
*\version 1.0
*\example Решение уравнений, работа с прогрессиями и массивами.
*
*/

/**
 * @brief printMenu
 * @return
 *@code
int m = 0;
char c = '\0';
printf("--------------------------------------------------------------------------\n");
printf("| 1) ax + b = 0                                                            |\n");
printf("| 2) ax^2 + bx + c = 0                                                     |\n");
printf("| 3) MIN element of the array                                              |\n");
printf("| 4) Find the i-th member of the arithmetic progression                    |\n");
printf("| 5) Find the amount of arithmetic progression                             |\n");
printf("| 6) Find the sum of a geometric progression                               |\n");
printf("| 7) Exit                                                                  |\n");
printf("--------------------------------------------------------------------------\n");
m = check(0, 8);
 *@endcode
 */


int printMenu()
{
    int m = 0;
    char c = '\0';
    printf("--------------------------------------------------------------------------\n");
    printf("| 1) ax + b = 0                                                            |\n");
    printf("| 2) ax^2 + bx + c = 0                                                     |\n");
    printf("| 3) MIN element of the array                                              |\n");
    printf("| 4) Find the i-th member of the arithmetic progression                    |\n");
    printf("| 5) Find the sum of a arithmetic progression                             |\n");
    printf("| 6) Find the sum of a geometric progression                               |\n");
    printf("| 7) Exit                                                                  |\n");
    printf("--------------------------------------------------------------------------\n");
    m = check(0, 8);

    return m;
}


/**
 * @brief main
 * @param argc
 * @param argv
 * @return
 * @code
 *   setlocale(LC_ALL,"Russian");
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    QCoreApplication a(argc, argv);

    int menu = 0;
    while(menu != 7)
    {
        menu = printMenu();
        switch(menu)
        {
        case 1:{
            system("cls");
            int a = 0;
            int b = 0;
            float x = 0;
            printf("Enter a: ");
            a = check(-99999, 99999);
            printf("Enter b: ");
            b = check(-99999, 99999);
            x = linearEq(a, b);
            printf("Root of the equation : x = %0.2f\n", x);
            break;
        }
        case 2:{
            system("cls");
            float* x = new float[2];
            int a = 0;
            int b = 0;
            int c = 0;
            printf("Enter a: ");
            a = check(-99999, 99999);
            printf("Enter b: ");
            b = check(-99999, 99999);
            printf("Enter c: ");
            c = check(-99999, 99999);
            x = quadrEq(a, b, c);
            if(a == 0)
            {
                printf("Root of the equation : x1 = %0.2f\n", x[0]);
                break;
            }
            printf("Root of the equation : x1 = %0.2f\n", x[0]);
            printf("Root of the equation : x2 = %0.2f\n", x[1]);
            break;
        }
        case 3:{
            system("cls");
            int i = 0;
            int n = 0;
            int ch = 0;
            printf("Enter array size :\n");
            n = check(0, 99999);
            int *a = new int[n];
            printf("Do you want to fill the array with random numbers (1) or not (2)?");
            ch = check(0,3);
            if(ch == 1)
            {
                printf("Array elements :\n");
                for (int i = 0; i < n; i++)
                {
                    a[i] = rand() % 201 - 100;
                }
                for (int i = 0; i < n; i++)
                {
                    printf("a[%d] = %d\n", i, a[i]);
                }
            }
            else
            {
                printf("Add array element :\n");
                for (int i = 0; i < n; i++)
                {
                    printf("a[%d] = ", i);
                    a[i] = check(-9999999, 9999999);
                }
            }
            i = arrayMin(a, n);
            printf("Index of arrays MIN element : %d\n", i);

            break;
        }
        case 4:{
            system("cls");
            int a = 0;
            int a1 = 0;
            int d = 0;
            int i = 0;
            int ch = 0;
            printf("Enter i: ");
            i = check(0, 99999);
            printf("Do you want to set a1 and d random values (1) or not (2)?");
            ch = check(0,3);
            if(ch == 1)
            {
                a1 = rand() % 201 - 100;
                d = rand() % 201 - 100;
                printf("a1 = %d\n", a1);
                printf("d = %d\n", d);
            }
            else
            {
                printf("Enter a1: ");
                a1 = check(-99999, 99999);
                printf("Enter d: ");
                d = check(-99999, 99999);
            }
            a = progressEl(a1, d, i);
            printf("Element a%d = %d\n", i, a);
            break;
        }
        case 5:{
            system("cls");
            int s = 0;
            int a1 = 0;
            int d = 0;
            int i = 0;
            int ch = 0;
            printf("Size (i): ");
            i = check(0, 99999);
            printf("Do you want to set a1 and d random values (1) or not (2)?");
            ch = check(0,3);
            if(ch == 1)
            {
                a1 = rand() % 201 - 100;
                d = rand() % 201 - 100;
                printf("a1 = %d\n", a1);
                printf("d = %d\n", d);
            }
            else
            {
                printf("Enter a1: ");
                a1 = check(-99999, 99999);
                printf("Enter d: ");
                d = check(-99999, 99999);
            }
            s = amountAr(a1, d, i);
            printf("Amount s%d = %d\n", i, s);
            break;
        }
        case 6:{
            system("cls");
            int s = 0;
            int b1 = 0;
            int q = 0;
            int i = 0;
            int ch = 0;
            printf("Size (i): ");
            i = check(0, 99999);
            printf("Do you want to set a1 and d random values (1) or not (2)?");
            ch = check(0,3);
            if(ch == 1)
            {
                b1 = rand() % 201 - 100;
                q = rand() % 201 - 100;
                printf("a1 = %d\n", b1);
                printf("d = %d\n", q);
            }
            else
            {
                printf("Enter b1: ");
                b1 = check(-99999, 99999);
                printf("Enter q: ");
                q = check(-99999, 99999);
            }
            s = amountG(b1, q, i);
            printf("Amount s%d = %d\n", i, s);
            break;
        }

    }

    }


return 0;
return a.exec();
 * @endcode
 */

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int menu = 0;
    while(menu != 7)
    {
        menu = printMenu();
        switch(menu)
        {
        case 1:{
            system("cls");
            int a = 0;
            int b = 0;
            float x = 0;
            printf("Enter a: ");
            a = check(-99999, 99999);
            printf("Enter b: ");
            b = check(-99999, 99999);
            x = linearEq(a, b);
            printf("Root of the equation : x = %0.2f\n", x);
            break;
        }
        case 2:{
            system("cls");
            float* x = new float[2];
            int a = 0;
            int b = 0;
            int c = 0;
            printf("Enter a: ");
            a = check(-99999, 99999);
            printf("Enter b: ");
            b = check(-99999, 99999);
            printf("Enter c: ");
            c = check(-99999, 99999);
            x = quadrEq(a, b, c);
            if(a == 0)
            {
                printf("Root of the equation : x1 = %0.2f\n", x[0]);
                break;
            }
            printf("Root of the equation : x1 = %0.2f\n", x[0]);
            printf("Root of the equation : x2 = %0.2f\n", x[1]);
            break;
        }
        case 3:{
            system("cls");
            int i = 0;
            int n = 0;
            int ch = 0;
            printf("Enter array size :\n");
            n = check(0, 99999);
            int *a = new int[n];
            printf("Do you want to fill the array with random numbers (1) or not (2)?");
            ch = check(0,3);
            if(ch == 1)
            {
                printf("Array elements :\n");
                for (int i = 0; i < n; i++)
                {
                    a[i] = rand() % 201 - 100;
                }
                for (int i = 0; i < n; i++)
                {
                    printf("a[%d] = %d\n", i, a[i]);
                }
            }
            else
            {
                printf("Add array element :\n");
                for (int i = 0; i < n; i++)
                {
                    printf("a[%d] = ", i);
                    a[i] = check(-9999999, 9999999);
                }
            }
            i = arrayMin(a, n);
            printf("Index of arrays MIN element : %d\n", i);

            break;
        }
        case 4:{
            system("cls");
            int a = 0;
            int a1 = 0;
            int d = 0;
            int i = 0;
            int ch = 0;
            printf("Enter i: ");
            i = check(0, 99999);
            printf("Do you want to set a1 and d random values (1) or not (2)?");
            ch = check(0,3);
            if(ch == 1)
            {
                a1 = rand() % 201 - 100;
                d = rand() % 201 - 100;
                printf("a1 = %d\n", a1);
                printf("d = %d\n", d);
            }
            else
            {
                printf("Enter a1: ");
                a1 = check(-99999, 99999);
                printf("Enter d: ");
                d = check(-99999, 99999);
            }
            a = progressEl(a1, d, i);
            printf("Element a%d = %d\n", i, a);
            break;
        }
        case 5:{
            system("cls");
            int s = 0;
            int a1 = 0;
            int d = 0;
            int i = 0;
            int ch = 0;
            printf("Size (i): ");
            i = check(0, 99999);
            printf("Do you want to set a1 and d random values (1) or not (2)?");
            ch = check(0,3);
            if(ch == 1)
            {
                a1 = rand() % 201 - 100;
                d = rand() % 201 - 100;
                printf("a1 = %d\n", a1);
                printf("d = %d\n", d);
            }
            else
            {
                printf("Enter a1: ");
                a1 = check(-99999, 99999);
                printf("Enter d: ");
                d = check(-99999, 99999);
            }
            s = amountAr(a1, d, i);
            printf("Amount s%d = %d\n", i, s);
            break;
        }
        case 6:{
            system("cls");
            int s = 0;
            int b1 = 0;
            int q = 0;
            int i = 0;
            int ch = 0;
            printf("Size (i): ");
            i = check(0, 99999);
            printf("Do you want to set a1 and d random values (1) or not (2)?");
            ch = check(0,3);
            if(ch == 1)
            {
                b1 = rand() % 201 - 100;
                q = rand() % 201 - 100;
                printf("a1 = %d\n", b1);
                printf("d = %d\n", q);
            }
            else
            {
                printf("Enter b1: ");
                b1 = check(-99999, 99999);
                printf("Enter q: ");
                q = check(-99999, 99999);
            }
            s = amountG(b1, q, i);
            printf("Amount s%d = %d\n", i, s);
            break;
        }

    }

    }


return 0;
return a.exec();
}
